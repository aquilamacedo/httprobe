#!/bin/bash

set -e

DOMAINS_PATH='debian/tests/source-data/domains.txt'
HTTPROBE_EXPECTED_OUTPUT='debian/tests/output/expected_output.txt'

# Perform httprobe on domains
httprobe_output="$(cat ${DOMAINS_PATH} | httprobe | sort)"

if [ $? -ne 0 ]; then
  exit 77
fi

# Sort the expected output for consistent comparison
expected_output=$(sort "${HTTPROBE_EXPECTED_OUTPUT}")

# Compare the sorted httprobe output with the sorted expected output
if [ "$httprobe_output" == "$expected_output" ]; then
    echo "Output matches the content of the file!"
else
    echo "Output does not match the expected content."
    exit 1
fi
